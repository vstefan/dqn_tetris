# Tetris with deep reinforcement learning
Implementation of a deep reinforcement learning model to play Tetris using the Keras library.
More details can be found in *report.pdf*

To run the model you can simply use:
```
python run.py
```
To train a new model, you can use:
```
python train.py [training_episodes]
```
where **training_episodes** specifies the number of training epochs that the agent goes through.
